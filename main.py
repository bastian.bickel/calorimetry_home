from functions import m_json
from functions import m_pck


#m_pck.check_sensors()

#path = ""
#metadata = m_json.get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json')

#m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets/', metadata)


#data = m_pck.get_meas_data_calorimetry(metadata)

#m_pck.logging_calorimetry(data, metadata, '/home/pi/calorimetry_home/data/Wärmekapazität', '/home/pi/calorimetry_home/datasheets')

#m_json.archiv_json('/home/pi/calorimetry_home', '/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json', '/home/pi/calorimetry_home/data/Wärmekapazität')




m_pck.check_sensors()

path = ""
metadata = m_json.get_metadata_from_setup('/home/pi/calorimetry_home/datasheets/setup_newton.json')


m_json.add_temperature_sensor_serials('/home/pi/calorimetry_home/datasheets/', metadata)


data = m_pck.get_meas_data_calorimetry(metadata)

m_pck.logging_calorimetry(data, metadata, '/home/pi/calorimetry_home/data/thermische_Verluste', '/home/pi/calorimetry_home/datasheets')

m_json.archiv_json('/home/pi/calorimetry_home', '/home/pi/calorimetry_home/datasheets/setup_newton.json', '/home/pi/calorimetry_home/data/thermische_Verluste')
